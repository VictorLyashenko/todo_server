const http = require('http');
const fs = require('fs');

function httpCallback (request, response) {

    response.writeHead(200, {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'DELETE, PUT, OPTIONS'
    });

    let requestURL = request.url;

    let parametesIndex = request.url.indexOf('?');
    let clearURL = '';
    if(parametesIndex !== -1){
        clearURL = requestURL.substring(0, parametesIndex);
    } else {
        clearURL = request.url;
    }

    if (clearURL === '/api') {
        if(request.method === 'OPTIONS'){
            response.end();
            return;
        }


    if (request.method === 'GET') {
        let todosObjBuffer = fs.readFileSync('scripts/todos.json');
        let todosObj = JSON.parse(todosObjBuffer);

        let todosStr = JSON.stringify(todosObj);
        response.end(todosStr);
        return;
    }
    if (request.method === 'POST') {
        request.on('data', (data) => {
            let todosObjBuffer = fs.readFileSync('scripts/todos.json');
            let todosObj = JSON.parse(todosObjBuffer);
            let cardObj = JSON.parse(data);

            let responseObj = {};

            let id = Object.keys(cardObj)[0];
            let ids = Object.keys(todosObj);

            if (ids.indexOf(id) === -1) {
                todosObj[id] = cardObj[id];
                let strData = JSON.stringify(todosObj);

                fs.writeFileSync('scripts/todos.json', strData)

                responseObj = {
                    status: 0,
                    message: 'Card was added'
                }
            } else {
                responseObj = {
                    status: 1,
                    message: 'Card exisist'
                }
            }

            let strResponse = JSON.stringify(responseObj);
            response.end(strResponse);
        });
        return;
    }
    if (request.method === 'DELETE') {

        let parametesIndex = request.url.indexOf('=');

        let cardId = requestURL.substring(parametesIndex + 1, request.url.length);
        console.log(cardId);

        let todosObjBuffer = fs.readFileSync('scripts/todos.json');
            let todosObj = JSON.parse(todosObjBuffer);

            let responseObj = {};

            let ids = Object.keys(todosObj);

            if (ids.indexOf(cardId) !== -1) {
                delete todosObj[cardId];
                let strData = JSON.stringify(todosObj);

                fs.writeFileSync('scripts/todos.json', strData)

                responseObj = {
                    status: 0,
                    message: 'Card was deleted'
                }
            } else {
                responseObj = {
                    status: 1,
                    message: 'Card doesn\'t exisist'
                }
            }
            console.log(request);

            let strResponse = JSON.stringify(responseObj);
            response.end(strResponse);
            return
        }
    if (request.method === 'PUT') {
        request.on('data', (data) => {
            let todosObjBuffer = fs.readFileSync('scripts/todos.json');
            let todosObj = JSON.parse(todosObjBuffer);
            let cardObj = JSON.parse(data);

            let responseObj = {};

            let id = Object.keys(cardObj)[0];
            let ids = Object.keys(todosObj);

            if (ids.indexOf(id) !== -1) {
                todosObj[id] = cardObj[id];
                let strData = JSON.stringify(todosObj);

                fs.writeFileSync('scripts/todos.json', strData)

                responseObj = {
                    status: 0,
                    message: 'Card was updated'
                }
            } else {
                responseObj = {
                    status: 1,
                    message: 'Card doesn\'t exisist'
                }
            }

            let strResponse = JSON.stringify(responseObj);
            response.end(strResponse);
        });
        return;
    }
        response.end('Welcome to API')
    } else {
        response.end('Page not found')
    }
}

http.createServer(httpCallback).listen(3009);
console.log('Server listening on port 3009');