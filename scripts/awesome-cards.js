'use strict';

class Card {
	constructor (config){
		this.config = config;

		this.api = 'http://localhost:3009/api';

		this.id = null;
		this.LS = localStorage;
		this.cardsData = this.LS.getItem('cards');

		this.parseCardsData = JSON.parse(this.cardsData);

		this._cardTitle = '';
		this._cardText = '';
		this._cardImportant = false;

		this._cardBlock = null;
		this._cardButton = null;
		this._titleEl = null;
		this._textEl = null;
		this._importantEl = null;
		this._deleteButtonEl = null;

		this._cardsBlock = null;
		this._changeTitleEl = null;
		this._changeTextEl = null;
		this._changeImportantEl = null;
		this._changeButtonEl = null;
	}

	_createElements() {
		this._cardBlock = document.createElement('div');
		this._cardButton = document.createElement('div');
		this._titleEl = document.createElement('h2');
		this._textEl = document.createElement('p');
		this._importantEl = document.createElement('input');
		this._deleteButtonEl = document.createElement('button');
		this._changeButtonEl = document.createElement('button');

		this._changeTitleEl = document.createElement('input');
		this._changeTextEl = document.createElement('input');
	}

	_addElements() {
		this._cardBlock.appendChild(this._titleEl);
		this._cardBlock.appendChild(this._changeTitleEl);
		this._cardBlock.appendChild(this._textEl);
		this._cardBlock.appendChild(this._changeTextEl);
		this._cardBlock.appendChild(this._importantEl);
		this._cardButton.appendChild(this._changeButtonEl);
		this._cardButton.appendChild(this._deleteButtonEl);

		this._cardsBlock.appendChild(this._cardBlock);
		this._cardBlock.appendChild(this._cardButton);
	}

	_addElementsContent() {
		this._cardBlock.classList.add('card-block');
		this._cardButton.classList.add('card-button');
		this._titleEl.textContent = this._cardTitle;
		this._textEl.textContent = this._cardText;
		this._deleteButtonEl.textContent = 'DELETE';
		this._changeButtonEl.textContent = 'CHANGE';
		this._changeButtonEl.setAttribute('data-action', 'change');

		this._importantEl.setAttribute('type', 'checkbox');
		this._importantEl.classList.add('important');
		if(this._cardImportant) {
			this._importantEl.setAttribute('checked', true);
		};

		this._changeTitleEl.style.display = 'none';
		this._changeTextEl.style.display = 'none';
	}

	_generateCard() {
		this._saveState();
	}

	_saveState() {
		if(!this.config.id) {
			this._generateId();
			this._updateState();
		} else {
			this.id = this.config.id;
		};
	}

	_updateState(method) {
		if(method !== 'change'){
			let card = {
				titles: this._cardTitle,
				text: this._cardText,
				important: this._cardImportant
			};
			//SERVER
			this._sendCard({[this.id]: card});
		}

		//LS
		if (this.cardsData !== null) {
			this.parseCardsData[this.id] = card;

			let strCardsData = JSON.stringify(this.parseCardsData);

			this.LS.setItem('cards', strCardsData);
		} else {
			this.parseCardsData = {};
			this.parseCardsData[this.id] = card;

			let strCardsData = JSON.stringify(this.parseCardsData);

			this.LS.setItem('cards', strCardsData);
		};
	}

	_sendCard(cardsData){
		let request = fetch(this.api,{
			method: 'POST',
			body: JSON.stringify(cardsData)
		});

		request.then((response) => {
			response.json().then((result) => {
				if(result.status === 0) {
					console.log('Cards was added');
					this._createElements();
					this._addElementsContent();
					this._addElements();
					this._attachEvents();
				} else {
					console.log('Error');
				}

			})
		}).catch((e) => {
			alert('Erorr');
		})
	}

	_getData() {
		this._cardTitle = this.config.titles;
		this._cardText = this.config.text;
		this._cardImportant = this.config.important;
	}

	_changeCard() {
		this._titleEl.style.display = 'none';
		this._textEl.style.display = 'none';

		this._changeTitleEl.style.display = 'block';
		this._changeTextEl.style.display = 'block';

		this._changeTitleEl.value = this._cardTitle;
		this._changeTextEl.value = this._cardText;

		this._changeButtonEl.textContent = 'SAVE';
		this._changeButtonEl.setAttribute('data-action', 'save');
	}

	_saveCard() {
		let card = {
			titles: this._changeTitleEl.value,
			text: this._changeTextEl.value,
			important: this._cardImportant
		};

		let request = fetch(this.api,{
			method: 'PUT',
			body: JSON.stringify({[this.id]: card})
		});

		request.then((response) => {
			response.json().then((result) => {
				if(result.status === 0) {
					console.log('Cards was updated');
					this._titleEl.textContent = this._changeTitleEl.value;
					this._textEl.textContent = this._changeTextEl.value;

					this._cardTitle = this._changeTitleEl.value;
					this._cardText = this._changeTextEl.value;

					this._changeTitleEl.style.display = 'none';
					this._changeTextEl.style.display = 'none';

					this._titleEl.style.display = 'block';
					this._textEl.style.display = 'block';

					this._changeButtonEl.textContent = 'CHANGE';
					this._changeButtonEl.setAttribute('data-action', 'change');

					this._updateState('change');
				} else {
					console.log('Error');
				}
			})
		}).catch((e) => {
			alert('Erorr');
		})
	}

	_attachEvents() {
		this._deleteButtonEl.addEventListener('click', (event) => {
			event.preventDefault();

			this.cardsData = this.LS.getItem('cards');
			this.parseCardsData = JSON.parse(this.cardsData);

			delete this.parseCardsData[this.id];
			const strCardsData = JSON.stringify(this.parseCardsData);

			let request = fetch(`${this.api}?id=${[this.id]}`, {
				method: 'DELETE'
			});

			request.then((response) => {
				response.json().then((result) => {
					if(result.status === 0) {
						console.log('Cards was deleted');
						this._cardsBlock.removeChild(this._cardBlock);
					} else {
						console.log('Card doesn\'t exsist');
					}
				})
			}).catch((e) => {
				alert('Erorr');
			});
			this.LS.setItem('cards', strCardsData);
		});

		this._changeButtonEl.addEventListener('click', (event) => {
			event.preventDefault();
			let action = this._changeButtonEl.getAttribute('data-action');

			switch (action) {
				case 'change':
					this._changeCard();
					break;
				case 'save':
					this._saveCard();
					break;
			};
		});

		this._importantEl.addEventListener('click', (event) => {
			 if (this._cardImportant) {
	        	this._cardImportant = false;
		    } else if (!this._cardImportant){
		        this._cardImportant = true;
		    };
		});
	}

	_generateId() {
		this.id = new Date().getTime();
	}

	init(sourse) {
		this._cardsBlock = document.querySelector('#cardsBlock');

		this._getData();
		this._generateCard();

		if(sourse === 'server') {

			this._createElements();
			this._addElementsContent();
			this._addElements();
			this._attachEvents();
		}
	}
}